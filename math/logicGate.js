function AND(input1, input2) {
    andCounter++;
    return bInt(input1 && input2);
}

function NOT(input1) {
    notCounter++;
    return bInt(!input1);
}

function OR(input1, input2) {
    var output1 = NOT(input1);
    var output2 = NOT(input2);
    
    var output3 = AND(output1, output2);
    
    return NOT(output3);
}

function NOR(input1, input2) {
    var output1 = OR(input1, input2);
    return NOT(output1);
}

function NAND(input1, input2) {
    var output1 = AND(input1, input2);
    return NOT(output1);
}

function XOR(input1, input2) {
    var output1 = OR(input1, input2);
    var output2 = AND(input1, input2);
    
    return AND(output1, NOT(output2));
}

function XNOR(input1, input2) {
    var output1 = XOR(input1, input2);
    return NOT(output1);
}

function ADDER(a, b, cIn) {
    var output1 = XOR(a, b);
    var sum = XOR(output1, cIn);
    
    var output2 = AND(a, b);
    var output3 = AND(output1, cIn);
    
    var carryOut = OR(output3, output2);
    
    return {
        sum: sum,
        sumAsString: sum.toString(),
        sumAsInt: parseInt(sum, 2),
        carryOut: carryOut
    };
    
}

function TWOBITMULTIPLY(a, b, cIn) {
    var output1 = AND(a[1], b[1]);
    var output2 = AND(a[0], b[1]);
    
    var output3 = AND(a[1], b[0]);
    var output4 = AND(a[0], b[0]);
    
    var addOut0 = ADDER(output2, output3, cIn);
    var addOut2 = ADDER(addOut0.carryOut, output4);
    
    var sum = [addOut2.carryOut, addOut2.sum, addOut0.sum, output1];
    var sumAsCalc = parseInt(a.join(""), 2) * parseInt(b.join(""), 2);
    return {
        sum: sum,
        sumAsInt: parseInt(sum.join(""), 2),
        sumAsCalc: sumAsCalc,
        calcAsBinary: toBinary(sumAsCalc, 4),
        sumAsString: sum.join("")
        
    };
}

function SUBTRACT(a, b, bIn) {
    var output1 = XOR(a,b);
    var diff = XOR(output1, bIn);
    
    var output2 = AND(NOT(a), b);
    var output3 = AND(NOT(output1), bIn);
    
    var borrowOut = OR(output3, output2);
    return {
        diff: diff,
        borrowOut: borrowOut
    };
}