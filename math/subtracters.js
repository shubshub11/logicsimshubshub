/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */

/* global notCounter, andCounter */

function FOURBITSUBTRACT(a, b, bIn) {
    
    var output1 = SUBTRACT(a[3], b[3], bIn);
    var output2 = SUBTRACT(a[2], b[2], output1.borrowOut);
    var output3 = SUBTRACT(a[1], b[1], output2.borrowOut);
    var output4 = SUBTRACT(a[0], b[0], output3.borrowOut);
    
    var diff = [output4.diff, output3.diff, output2.diff, output1.diff];
    var diffAsCalc = parseInt(a.join(""), 2) - parseInt(b.join(""), 2);
    var borrowOut = output4.borrowOut;
    return {
        input1AsInt: parseInt(a.join(""), 2),
        input2AsInt: parseInt(b.join(""), 2),
        diff: diff,
        
        diffAsInt: parseInt(diff.join(""), 2),
        diffAsCalc: diffAsCalc,
        calcAsBinary: toBinary(diffAsCalc, 4),
        diffAsString: diff.join(""),
        underMax: underMax(diffAsCalc, 0),
        
        borrowOut: borrowOut,
        andCount: andCounter,
        notCount: notCounter
    };
}

function EIGHTBITSUBTRACT(a, b, bIn) {
    //[0,0,0,0,0,0,0,0]
    
    var inputArray1 = [a[0], a[1], a[2], a[3]];
    var inputArray2 = [b[0], b[1], b[2], b[3]];
    
    var inputArray3 = [a[4], a[5], a[6], a[7]];
    var inputArray4 = [b[4], b[5], b[6], b[7]];
    
    var output2 = FOURBITSUBTRACT(inputArray3, inputArray4, bIn);
    var output1 = FOURBITSUBTRACT(inputArray1, inputArray2, output2.borrowOut);

    
    
    var diff = [output1.diff[0], output1.diff[1], output1.diff[2], output1.diff[3], output2.diff[0], output2.diff[1], output2.diff[2], output2.diff[3]];
    var borrowOut = output1.borrowOut;
    var diffAsCalc = parseInt(a.join(""), 2) - parseInt(b.join(""), 2);
    return {
        input1AsInt: parseInt(a.join(""), 2),
        input2AsInt: parseInt(b.join(""), 2),
        diff: diff,
        
        diffAsInt: parseInt(diff.join(""), 2),
        diffAsCalc: diffAsCalc,
        calcAsBinary: toBinary(diffAsCalc, 8),
        diffAsString: diff.join(""),
        underMax: underMax(diffAsCalc, 0),
        borrowOut: borrowOut,
        andCount: andCounter,
        notCount: notCounter
    };   
}

function SIXTEENBITSUBTRACT(a, b, bIn) {
    //[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    
    var inputArray1 = [a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]];
    var inputArray2 = [b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7]];
    
    var inputArray3 = [a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]];
    var inputArray4 = [b[8], b[9], b[10], b[11], b[12], b[13], b[14], b[15]];
    
    var output2 = EIGHTBITSUBTRACT(inputArray3, inputArray4, bIn);
    var output1 = EIGHTBITSUBTRACT(inputArray1, inputArray2, output2.borrowOut);
    

    var diff = [output1.diff[0], output1.diff[1], output1.diff[2], output1.diff[3], output1.diff[4], output1.diff[5], output1.diff[6], output1.diff[7], output2.diff[0], output2.diff[1], output2.diff[2], output2.diff[3], output2.diff[4], output2.diff[5], output2.diff[6], output2.diff[7]];
    var borrowOut = output1.borrowOut;
    var diffAsCalc = parseInt(a.join(""), 2) - parseInt(b.join(""), 2);
    return {
        input1AsInt: parseInt(a.join(""), 2),
        input2AsInt: parseInt(b.join(""), 2),
        diff: diff,
        diffAsInt: parseInt(diff.join(""), 2),
        diffAsCalc: diffAsCalc,
        calcAsBinary: toBinary(diffAsCalc, 16),
        diffAsString: diff.join(""),
        underMax: underMax(diffAsCalc, 0),
        borrowOut: borrowOut,
        andCount: andCounter,
        notCount: notCounter
    };   
}