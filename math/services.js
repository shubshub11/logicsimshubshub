/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */
function overMax(sum, maxSum) {
    if (sum > maxSum) {
        return true;
    }
    return false;
}

function underMax(diff, maxDiff) {
    if (diff < maxDiff) {
        return true;
    }
    return false;
}

function toBinary(dec, digits) {
    
  var data = ((dec >>> 0).toString(2)).split("");
  
  while(data.length < digits) {
      data.unshift("0");
  }
    
  return data.map(function (x) { return parseInt(x, 10); });
  
}

function reset() {
    andCounter = 0;
    notCounter = 0;
}

function bInt(data) {
    if (data === true) {
        return 1;
    } else if (data === false) {
        return 0;
    }
    
    return data;
}

function specialRandom() {
    var y = Math.random();
    if (y < 0.5)
      y = 0
    else
      y= 1
  
  return y;
}

function randomBinaryDigitArray(size) {
    var array = [];
    for ( var i = 0; i < size; i++) {
        array.push(specialRandom() );
    }
    return array;
}

function everyBitAdderTest(tests) {
    for (var i = 0; i <= tests; i++) {
        
        var out2 = THIRTYTWOBITADDER(randomBinaryDigitArray(32), randomBinaryDigitArray(32), 0);
        if (out2.sumAsCalc != out2.sumAsInt && !out2.overMax) {
            console.log("16bit: ", out2);
        }
    }
}

function everyBitSubtractTest(tests) {
    for (var i = 0; i <= tests; i++) {
        
        var out2 = SIXTEENBITSUBTRACT(randomBinaryDigitArray(16), randomBinaryDigitArray(16), 0);
        if ((out2.diffAsCalc != out2.diffAsInt) && !out2.underMax) {
            console.log("8bit: ", out2);
        }
    }
}

var andCounter = 0;
var notCounter = 0;