/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */
function TWOBITMULTIPLY(a, b, cIn) {
    var output1 = AND(a[1], b[1]);
    var output2 = AND(a[0], b[1]);
    
    var output3 = AND(a[1], b[0]);
    var output4 = AND(a[0], b[0]);
    
    var addOut0 = ADDER(output2, output3, cIn);
    var addOut2 = ADDER(addOut0.carryOut, output4);
    
    var sum = [addOut2.carryOut, addOut2.sum, addOut0.sum, output1];
    var sumAsCalc = parseInt(a.join(""), 2) * parseInt(b.join(""), 2);
    var carryOut = addOut2.carryOut;
    
    return {
        sum: sum,
        sumAsInt: parseInt(sum.join(""), 2),
        carryOut: carryOut,
        sumAsCalc: sumAsCalc,
        calcAsBinary: toBinary(sumAsCalc, 4),
        sumAsString: sum.join("")
        
    };
}

function FOURBITMULTIPLY(a, b, cIn) {

    var inputArray2a = [a[0], a[1]];
    var inputArray2b = [b[0], b[1]];
    var inputArray1a = [a[2], a[3]];
    var inputArray1b = [b[2], b[3]];
    
    var addOut0 = TWOBITMULTIPLY(inputArray1a, inputArray1b);
    var addOut1 = TWOBITMULTIPLY(inputArray2a, inputArray1b, addOut0.carryOut);
    
    var addOut2 = TWOBITMULTIPLY(inputArray1a, inputArray2b, addOut1.carryOut);
    var addOut3 = TWOBITMULTIPLY(inputArray2a, inputArray2b, addOut2.carryOut);
    
    
    var addOut4 = FOURBITADDER(addOut0.sum, addOut1.sum, cIn);
    var addOut5 = FOURBITADDER(addOut2.sum, addOut3.sum, addOut4.carryOut);
    
    var sum = addOut4.sum.concat(addOut5.sum);
    
    
    
    var carryOut = addOut0.carryOut;
    
    console.log("Addout0: ", addOut0);
    console.log("Addout1: ", addOut1);
    console.log("AddOut2: ", addOut2);
    console.log("Addout3: ", addOut3);
    console.log("Addout4: ", addOut4);
    console.log("Addout5: ", addOut5);
    //var sum = [];
    var sumAsCalc = parseInt(a.join(""), 2) * parseInt(b.join(""), 2);
    return {
        sum: sum,
        sumAsInt: parseInt(sum.join(""), 2),
        sumAsCalc: sumAsCalc,
        calcAsBinary: toBinary(sumAsCalc, 8),
        carryOut: carryOut,
        sumAsString: sum.join("")
        
    };
}
