/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */
function FOURBITADDER(a, b, cIn) {
    //[0,0,0,0]
    var output1 = ADDER(a[3], b[3], cIn);
    var output2 = ADDER(a[2], b[2], output1.carryOut);
    var output3 = ADDER(a[1], b[1], output2.carryOut);
    var output4 = ADDER(a[0], b[0], output3.carryOut);
    
    var sum = [output4.sum, output3.sum, output2.sum, output1.sum];
    var sumAsCalc = parseInt(a.join(""), 2) + parseInt(b.join(""), 2);
    var carryOut = output4.carryOut;
    return {
        input1AsInt: parseInt(a.join(""), 2),
        input2AsInt: parseInt(b.join(""), 2),
        sum: sum,
        sumAsInt: parseInt(sum.join(""), 2),
        sumAsCalc: sumAsCalc,
        sumAsString: sum.join(""),
        
        overMax: overMax(sumAsCalc, 15),
        carryOut: carryOut,
        andCount: andCounter,
        notCount: notCounter
    };
}

function EIGHTBITADDER(a, b, cIn) {
    //[0,0,0,0,0,0,0,0]
    
    var inputArray1 = [a[0], a[1], a[2], a[3]];
    var inputArray2 = [b[0], b[1], b[2], b[3]];
    
    var inputArray3 = [a[4], a[5], a[6], a[7]];
    var inputArray4 = [b[4], b[5], b[6], b[7]];
    
    var output2 = FOURBITADDER(inputArray3, inputArray4, cIn);
    var output1 = FOURBITADDER(inputArray1, inputArray2, output2.carryOut);

    
    
    var sum = [output1.sum[0], output1.sum[1], output1.sum[2], output1.sum[3], output2.sum[0], output2.sum[1], output2.sum[2], output2.sum[3]];
    var carryOut = output1.carryOut;
    var sumAsCalc = parseInt(a.join(""), 2) + parseInt(b.join(""), 2);

    return {
        input1AsInt: parseInt(a.join(""), 2),
        input2AsInt: parseInt(b.join(""), 2),
        sum: sum,
        sumAsInt: parseInt(sum.join(""), 2),
        sumAsCalc: sumAsCalc,
        sumAsString: sum.join(""),
        
        overMax: overMax(sumAsCalc, 255),
        carryOut: carryOut,
        andCount: andCounter,
        notCount: notCounter
    };   
}

function SIXTEENBITADDER(a, b, cIn) {
    //[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    
    var inputArray1 = [a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]];
    var inputArray2 = [b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7]];
    
    var inputArray3 = [a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]];
    var inputArray4 = [b[8], b[9], b[10], b[11], b[12], b[13], b[14], b[15]];
    
    var output2 = EIGHTBITADDER(inputArray3, inputArray4, cIn);
    var output1 = EIGHTBITADDER(inputArray1, inputArray2, output2.carryOut);
    

    var sum = [output1.sum[0], output1.sum[1], output1.sum[2], output1.sum[3], output1.sum[4], output1.sum[5], output1.sum[6], output1.sum[7], output2.sum[0], output2.sum[1], output2.sum[2], output2.sum[3], output2.sum[4], output2.sum[5], output2.sum[6], output2.sum[7]];
    var carryOut = output1.carryOut;
    var sumAsCalc = parseInt(a.join(""), 2) + parseInt(b.join(""), 2);
    return {
        input1AsInt: parseInt(a.join(""), 2),
        input2AsInt: parseInt(b.join(""), 2),
        sum: sum,
        sumAsInt: parseInt(sum.join(""), 2),
        sumAsCalc: sumAsCalc,
        sumAsString: sum.join(""),
        
        overMax: overMax(sumAsCalc, 65535),
        carryOut: carryOut,
        andCount: andCounter,
        notCount: notCounter
    };   
}

function THIRTYTWOBITADDER(a, b, cIn) {
    //[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    
    var inputArray1 = [a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]];
    var inputArray2 = [b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7], b[8], b[9], b[10], b[11], b[12], b[13], b[14], b[15]];
    
    var inputArray3 = [a[16], a[17], a[18], a[19], a[20], a[21], a[22], a[23], a[24], a[25], a[26], a[27], a[28], a[29], a[30], a[31]];
    var inputArray4 = [b[16], b[17], b[18], b[19], b[20], b[21], b[22], b[23], b[24], b[25], b[26], b[27], b[28], b[29], b[30], b[31]];
    
    var output2 = SIXTEENBITADDER(inputArray3, inputArray4, cIn);
    var output1 = SIXTEENBITADDER(inputArray1, inputArray2, output2.carryOut);
    

    var sum = [output1.sum[0], output1.sum[1], output1.sum[2], output1.sum[3], 
                output1.sum[4], output1.sum[5], output1.sum[6], output1.sum[7], 
                output1.sum[8], output1.sum[9], output1.sum[10], output1.sum[11], 
                output1.sum[12], output1.sum[13], output1.sum[14], output1.sum[15],
                output2.sum[0], output2.sum[1], output2.sum[2], output2.sum[3], 
                output2.sum[4], output2.sum[5], output2.sum[6], output2.sum[7],
                output2.sum[8], output2.sum[9], output2.sum[10], output2.sum[11],
                output2.sum[12], output2.sum[13], output2.sum[14], output2.sum[15]];
    var carryOut = output2.carryOut;
    var sumAsCalc = parseInt(a.join(""), 2) + parseInt(b.join(""), 2);
    return {
        input1AsInt: parseInt(a.join(""), 2),
        input2AsInt: parseInt(b.join(""), 2),
        sum: sum,
        sumAsInt: parseInt(sum.join(""), 2),
        sumAsCalc: sumAsCalc,
        sumAsString: sum.join(""),
        
        overMax: overMax(sumAsCalc, 2147483647),
        carryOut: carryOut,
        andCount: andCounter,
        notCount: notCounter
    };   
}